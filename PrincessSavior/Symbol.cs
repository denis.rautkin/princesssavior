﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrincessSavior
{
    class Symbol
    {
        public const char Nothing = '\u0020'; //
        public const char UpperLeftCorner = '\u2554'; // ╔
        public const char BottomLeftCorner = '\u255a'; // ╚
        public const char UpperRightCorner = '\u2557'; // ╗ 
        public const char BottomRightCorner = '\u255d'; // ╝
        public const char HorizontalLine = '\u2550'; // ═
        public const char VerticalLine = '\u2551'; // ║
        public const char UpperCross = '\u2566'; // ╦
        public const char BottomCross = '\u2569'; // ╩
        public const char LeftCross = '\u2560'; // ╠
        public const char RightCross = '\u2563'; // ╣
        public const char OverCross = '\u256c'; // ╬
        public const char UpArrow = '\u2191'; // ↑
        public const char DownArrow = '\u2193'; // ↓
        public const char LeftArrow = '\u2190'; // ←
        public const char RightArrow = '\u2192'; // →
        public const char Apostrophe = '\u0027'; // '

        public const char Space = '\u0020'; // " "
        public const char SingleVerticalLine = '\u007c'; // |

        public const char Grass = '\u2591'; // ░
        public const char Player = '\u2689'; // ☻
        public const char Princess = '\u2655'; // ♕


    }
}