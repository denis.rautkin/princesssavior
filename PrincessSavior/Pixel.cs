﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrincessSavior
{
    public class Pixel
    {
        public char SymbolOfPixel { get; set; }
        public ConsoleColor SymbolColor { get; set; }
        public ConsoleColor BackgroundColor { get; set; }
        public PixelType TypeOfPixel { get; set; }

        public Pixel()
        {
            this.SymbolOfPixel = Symbol.Nothing;
            this.SymbolColor = ConsoleColor.White;
            this.BackgroundColor = ConsoleColor.Black;
            this.TypeOfPixel = PixelType.sprite;
        }

        public Pixel(char symbol, ConsoleColor symbolColor, ConsoleColor backgroundColor, PixelType typeOfPixel)
        {
            this.SymbolOfPixel = symbol;
            this.SymbolColor = symbolColor;
            this.BackgroundColor = backgroundColor;
            this.TypeOfPixel = typeOfPixel;
        }
    }
}
