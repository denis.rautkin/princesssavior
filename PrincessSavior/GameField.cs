﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PrincessSavior
{
    public class GameField
    {
        public int FieldWidth { get; set; }
        public int FieldHeight { get; set; }
        public int CellWidth { get; set; }
        public int CellHeight { get; set; }
        public int MinesCount { get; set; }
        public Cell[,] FieldCells { get; set; }

        public GameField()
        {
            this.FieldWidth = Settings.DefaultFieldWidth;
            this.FieldHeight = Settings.DefaultFieldHeight;
            this.CellWidth = Settings.DefaultCellWidth;
            this.CellHeight = Settings.DefaultCellHeight;
            this.MinesCount = Settings.DefaultMinesCount;
            this.FieldCells = new Cell[this.FieldHeight, this.FieldWidth];
            this.FillGameField();
        }

        public GameField(int fieldWidth, int fieldHeight, int cellWidth, int cellHeight, int minesCount)
        {
            this.FieldWidth = fieldWidth;
            this.FieldHeight = fieldHeight;
            this.CellWidth = cellWidth;
            this.CellHeight = cellHeight;
            this.MinesCount = minesCount;
            this.FieldCells = new Cell[this.FieldHeight, this.FieldWidth];
            this.FillGameField();
        }

        private void FillGameField()
        {
            ConsoleColor borderSymbolColor = Settings.DefaultBorderSymbolColor;
            ConsoleColor borderBackgroundColor = Settings.DefaultBorderBackgroundColor;

            for (int i = 0; i < FieldHeight; i++)
            {
                for (int j = 0; j < FieldWidth; j++)
                {
                    CellType cellType;
                    if (i == 0)
                    {
                        if (j == 0)
                        {
                            cellType = CellType.upperLeftCorner;
                        }
                        else if (j == (FieldWidth - 1))
                        {
                            cellType = CellType.upperRightCorner;
                        }
                        else
                        {
                            cellType = CellType.upper;
                        }
                    }
                    else if (i == (FieldHeight - 1))
                    {
                        if (j == 0)
                        {
                            cellType = CellType.bottomLeftCorner;
                        }
                        else if (j == (FieldWidth - 1))
                        {
                            cellType = CellType.bottomRightCorner;
                        }
                        else
                        {
                            cellType = CellType.bottom;
                        }
                    }
                    else
                    {
                        if (j == 0)
                        {
                            cellType = CellType.left;
                        }
                        else if (j == (FieldWidth - 1))
                        {
                            cellType = CellType.right;
                        }
                        else
                        {
                            cellType = CellType.ordinary;
                        }
                    }

                    FieldCells[i, j] = new Cell(this.CellWidth, this.CellHeight, borderSymbolColor, borderBackgroundColor, cellType);
                }

            }
        }

        public void RenderGameField(Player player)
        {
            Console.Clear();
            Console.CursorTop = Settings.DefaultCellHeight;
            Console.OutputEncoding = Encoding.Unicode;

            for (int i = 0; i < this.FieldHeight; i++)
            {
                for (int j = 0; j < this.CellHeight; j++)
                {
                    Console.CursorLeft = Settings.FieldsStartWidth;

                    for (int k = 0; k < this.FieldWidth; k++)
                    {
                        for (int l = 0; l < this.CellWidth; l++)
                        {
                            Cell currentCell = this.FieldCells[i, k];
                            Pixel currentPixel = currentCell.Content[j, l];

                            if ((currentCell.IsPlayersCurrentPosition) &&
                                (currentPixel.TypeOfPixel == PixelType.sprite))
                            {
                                //currentPixel.SymbolOfPixel = Symbol.Player;
                                Console.ForegroundColor = ConsoleColor.Blue;
                                Console.BackgroundColor = ConsoleColor.DarkYellow;
                            }
                            else if ((currentCell.IsPrincessPosition) &&
                                (currentPixel.TypeOfPixel == PixelType.sprite))
                            {
                                //currentPixel.SymbolOfPixel = Symbol.Princess;
                                Console.ForegroundColor = ConsoleColor.Magenta;
                                Console.BackgroundColor = ConsoleColor.Black;
                            }
                            else if ((!currentCell.IsPlayersCurrentPosition) &&
                                     (!currentCell.IsPrincessPosition) &&
                                     (currentPixel.TypeOfPixel == PixelType.sprite))
                            {
                                Console.ForegroundColor = ConsoleColor.Blue;
                                Console.BackgroundColor = ConsoleColor.Green;
                            }
                            else
                            {
                                Console.ForegroundColor = Settings.DefaultBorderSymbolColor;
                                Console.BackgroundColor = Settings.DefaultBorderBackgroundColor;
                            }

                            Console.Write(currentPixel.SymbolOfPixel);
                        }

                    }
                    Console.WriteLine();
                }
            }

            this.RenderMessageLine($"Use {Symbol.Apostrophe} {Symbol.UpArrow} {Symbol.Apostrophe}, {Symbol.Apostrophe} {Symbol.DownArrow} {Symbol.Apostrophe}, {Symbol.Apostrophe} {Symbol.LeftArrow} {Symbol.Apostrophe}, {Symbol.Apostrophe} {Symbol.RightArrow} {Symbol.Apostrophe} keys to move");

            this.RenderHealthStatus(player);
        }

        public void DropTraps(int playersHealth)
        {
            Random rand = new Random((int)(DateTime.Now.Ticks));

            for (int i = 0; i < this.MinesCount; i++)
            {
                int heightCoord = rand.Next(0, (this.FieldHeight - 1));
                int widthCoord = rand.Next(0, (this.FieldWidth - 1));

                Trap trap = new Trap();
                int damagePointValue = (int) Math.Ceiling((decimal)(playersHealth / 100));
                trap.DamagePoints = rand.Next(damagePointValue, (damagePointValue * 100));

                Cell randomCell = this.FieldCells[heightCoord, widthCoord];

                randomCell.Trap = trap;

            }
        }

        public void SetPlayersPosition(Player player)
        {
            Random rand = new Random((int)(DateTime.Now.Ticks));
            int heightCoord = rand.Next(0, (this.CellHeight - 1));
            int widthCoord = 0;
            player.CurrentHeightPosition = heightCoord;
            player.CurrentWidthPosition = widthCoord;
            this.FieldCells[heightCoord, widthCoord].IsPlayersCurrentPosition = true;
        }

        public void SetPrincessPosition()
        {
            Random rand = new Random((int)(DateTime.Now.Ticks));
            int heightCoord = rand.Next(0, (this.FieldHeight - 1));
            int widthCoord = this.FieldWidth - 1;
            this.FieldCells[heightCoord, widthCoord].IsPrincessPosition = true;
        }

        public void RenderMessageLine(string message)
        {
            this.SetMessagePosition();
            this.ClearLine(Console.CursorLeft, Console.CursorTop);
            this.SetMessagePosition();

            Console.Write(message);
        }

        public void RenderHealthStatus(Player player)
        {
            this.SetHealthStatusPosition();
            this.ClearLine(Console.CursorLeft, Console.CursorTop);
            this.SetHealthStatusPosition();

            StringBuilder message = new StringBuilder("Health: [");

            int damagePoint = (int) Math.Ceiling((decimal) player.Health / 10);

            for (int i = 1; i <= 10; i ++)
            {
                if (((player.Health - damagePoint) > player.CurrentHealth) || (player.CurrentHealth <= 0))
                {
                    message.Append(Symbol.Space).Append(Symbol.Space);
                }
                else
                {
                    message.Append(Symbol.Space).Append(Symbol.SingleVerticalLine);
                }
            }

            message.Append(" ]");

            Console.Write(message);
        }

        public void ClearLine(int startPosition, int heightPosition)
        {
            int lineLength = Console.WindowWidth - startPosition;
            StringBuilder emptyLine = new StringBuilder();

            for (int i = 0; i < lineLength; i++)
            {
                emptyLine.Append(Symbol.Space);
            }

            Console.CursorTop = heightPosition;
            Console.CursorLeft = startPosition;

            Console.Write(emptyLine);
        }

        public void SetMessagePosition()
        {
            Console.CursorTop = Settings.MessageHeightPosition;
            Console.CursorLeft = Settings.FieldsStartWidth + (this.FieldWidth * this.FieldCells[0, 0].Width) + 5;
        }

        public void SetHealthStatusPosition()
        {
            Console.CursorTop = Settings.HealthStatusHeightPosition;
            Console.CursorLeft = Settings.FieldsStartWidth + (this.FieldWidth * this.FieldCells[0, 0].Width) + 5;
        }

    }
}
