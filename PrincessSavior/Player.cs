﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrincessSavior
{
    public class Player
    {
        public int Health { get; set; }
        public int CurrentHealth { get; set; }
        public int CurrentHeightPosition { get; set; }
        public int CurrentWidthPosition { get; set; }

        public Player()
        {
            this.Health = Settings.DefaultHealthLevel;
            this.CurrentHealth = this.Health;
            this.CurrentHeightPosition = Settings.DefaultFieldHeight - 1;
            this.CurrentWidthPosition = 0;
        }

        public Player(int health, int currentHeightPosition, int currentWidthPosition)
        {
            this.Health = health;
            this.CurrentHealth = this.Health;
            this.CurrentHeightPosition = currentHeightPosition;
            this.CurrentWidthPosition = currentWidthPosition;
        }
    }
}
