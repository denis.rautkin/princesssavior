﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrincessSavior
{
    public class Cell
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public ConsoleColor BorderSymbolColor { get; set; }
        public ConsoleColor BorderBackgroundColor { get; set; }
        public CellType TypeOfCell { get; }
        public Pixel[,] Content { get; set; }
        public int EntryContentHeight { get; set; }
        public int EntryContentWidth { get; set; }
        public Trap Trap { get; set; }
        public bool IsPlayersCurrentPosition { get; set; }
        public bool IsPrincessPosition { get; set; }

        public Cell()
        {
            this.Width = Settings.DefaultCellWidth;
            this.Height = Settings.DefaultCellHeight;
            this.BorderSymbolColor = Settings.DefaultBorderSymbolColor;
            this.BorderBackgroundColor = Settings.DefaultBorderBackgroundColor;
            this.TypeOfCell = Settings.DefaultCellType;
            this.Content = new Pixel[this.Width, this.Height];
            this.EntryContentHeight = this.Height - 2;
            this.EntryContentWidth = this.Width - 2;
            FillCell();
        }

        public Cell(int width, int height, ConsoleColor borderSymbolColor, ConsoleColor borderBackgroundColor, CellType cellType)
        {
            this.Width = width;
            this.Height = height;
            this.BorderSymbolColor = borderSymbolColor;
            this.BorderBackgroundColor = borderBackgroundColor;
            this.TypeOfCell = cellType;
            this.Content = new Pixel[this.Height, this.Width];
            this.EntryContentHeight = this.Height - 2;
            this.EntryContentWidth = this.Width - 2;
            FillCell();
        }

        private void FillCell()
        {
            FillUpperBorder();
            FillBottomBorder();
            FillLeftBorder();
            FillRightBorder();
            FillEntryContent();
        }

        private void FillUpperBorder()
        {
            for (int i = 0; i < this.Width; i++)
            {
                switch (TypeOfCell)
                {
                    case CellType.ordinary:

                        if ((i == 0) || (i == (this.Width - 1)))
                        { 
                            Content[0, i] = new Pixel(Symbol.OverCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else
                        {
                            Content[0, i] = new Pixel(Symbol.HorizontalLine, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }

                        break;

                    case CellType.left:

                        if (i == 0)
                        {
                            Content[0, i] = new Pixel(Symbol.LeftCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else if (i == (this.Width - 1))
                        {
                            Content[0, i] = new Pixel(Symbol.OverCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else
                        {
                            Content[0, i] = new Pixel(Symbol.HorizontalLine, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }

                        break;

                    case CellType.right:

                        if (i == 0)
                        {
                            Content[0, i] = new Pixel(Symbol.OverCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else if (i == (this.Width - 1))
                        {
                            Content[0, i] = new Pixel(Symbol.RightCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else
                        {
                            Content[0, i] = new Pixel(Symbol.HorizontalLine, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }

                        break;

                    case CellType.upperLeftCorner:

                        if (i == 0)
                        {
                            Content[0, i] = new Pixel(Symbol.UpperLeftCorner, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else if (i == (this.Width - 1))
                        {
                            Content[0, i] = new Pixel(Symbol.UpperRightCorner, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else
                        {
                            Content[0, i] = new Pixel(Symbol.HorizontalLine, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }

                        break;

                    case CellType.upper:

                        if ((i == 0) || (i == (this.Width - 1)))
                        {
                            Content[0, i] = new Pixel(Symbol.UpperCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else
                        {
                            Content[0, i] = new Pixel(Symbol.HorizontalLine, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }

                        break;

                    case CellType.upperRightCorner:

                        if (i == 0)
                        {
                            Content[0, i] = new Pixel(Symbol.UpperCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else if (i == (this.Width - 1))
                        {
                            Content[0, i] = new Pixel(Symbol.UpperRightCorner, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else
                        {
                            Content[0, i] = new Pixel(Symbol.HorizontalLine, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }

                        break;

                    case CellType.bottomLeftCorner:

                        if (i == 0)
                        {
                            Content[0, i] = new Pixel(Symbol.LeftCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else if (i == (this.Width - 1))
                        {
                            Content[0, i] = new Pixel(Symbol.OverCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else
                        {
                            Content[0, i] = new Pixel(Symbol.HorizontalLine, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }

                        break;

                    case CellType.bottomRightCorner:

                        if (i == 0)
                        {
                            Content[0, i] = new Pixel(Symbol.OverCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else if (i == (this.Width - 1))
                        {
                            Content[0, i] = new Pixel(Symbol.RightCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else
                        {
                            Content[0, i] = new Pixel(Symbol.HorizontalLine, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }

                        break;

                    case CellType.bottom:

                        if ((i == 0) || (i == (this.Width - 1)))
                        {
                            Content[0, i] = new Pixel(Symbol.OverCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else
                        {
                            Content[0, i] = new Pixel(Symbol.HorizontalLine, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }

                        break;

                    default:

                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private void FillBottomBorder()
        {
            int lastRow = (this.Height - 1);

            for (int i = 0; i < this.Width; i++)
            {
                switch (TypeOfCell)
                {
                    case CellType.ordinary:

                        if ((i == 0) || (i == (this.Width - 1)))
                        {
                            Content[lastRow, i] = new Pixel(Symbol.OverCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else
                        {
                            Content[lastRow, i] = new Pixel(Symbol.HorizontalLine, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }

                        break;

                    case CellType.left:

                        if (i == 0)
                        {
                            Content[lastRow, i] = new Pixel(Symbol.LeftCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else if (i == (this.Width - 1))
                        {
                            Content[lastRow, i] = new Pixel(Symbol.OverCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else
                        {
                            Content[lastRow, i] = new Pixel(Symbol.HorizontalLine, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }

                        break;

                    case CellType.right:

                        if (i == 0)
                        {
                            Content[lastRow, i] = new Pixel(Symbol.OverCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else if (i == (this.Width - 1))
                        {
                            Content[lastRow, i] = new Pixel(Symbol.RightCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else
                        {
                            Content[lastRow, i] = new Pixel(Symbol.HorizontalLine, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }

                        break;

                    case CellType.upperLeftCorner:

                        if (i == 0)
                        {
                            Content[lastRow, i] = new Pixel(Symbol.LeftCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else if (i == (this.Width - 1))
                        {
                            Content[lastRow, i] = new Pixel(Symbol.OverCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else
                        {
                            Content[lastRow, i] = new Pixel(Symbol.HorizontalLine, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }

                        break;

                    case CellType.upper:

                        if ((i == 0) || (i == (this.Width - 1)))
                        {
                            Content[lastRow, i] = new Pixel(Symbol.OverCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else
                        {
                            Content[lastRow, i] = new Pixel(Symbol.HorizontalLine, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }

                        break;

                    case CellType.upperRightCorner:

                        if (i == 0)
                        {
                            Content[lastRow, i] = new Pixel(Symbol.OverCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else if (i == (this.Width - 1))
                        {
                            Content[lastRow, i] = new Pixel(Symbol.RightCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else
                        {
                            Content[lastRow, i] = new Pixel(Symbol.HorizontalLine, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }

                        break;

                    case CellType.bottomLeftCorner:

                        if (i == 0)
                        {
                            Content[lastRow, i] = new Pixel(Symbol.BottomLeftCorner, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else if (i == (this.Width - 1))
                        {
                            Content[lastRow, i] = new Pixel(Symbol.BottomCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else
                        {
                            Content[lastRow, i] = new Pixel(Symbol.HorizontalLine, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }

                        break;

                    case CellType.bottomRightCorner:

                        if (i == 0)
                        {
                            Content[lastRow, i] = new Pixel(Symbol.BottomCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else if (i == (this.Width - 1))
                        {
                            Content[lastRow, i] = new Pixel(Symbol.BottomRightCorner, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else
                        {
                            Content[lastRow, i] = new Pixel(Symbol.HorizontalLine, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }

                        break;

                    case CellType.bottom:

                        if ((i == 0) || (i == (this.Width - 1)))
                        {
                            Content[lastRow, i] = new Pixel(Symbol.BottomCross, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }
                        else
                        {
                            Content[lastRow, i] = new Pixel(Symbol.HorizontalLine, this.BorderSymbolColor, this.BorderBackgroundColor,
                                PixelType.border);
                        }

                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private void FillLeftBorder()
        {
            for (int i = 1; i < (this.Height - 1); i++)
            {
                Content[i, 0] = new Pixel(Symbol.VerticalLine, this.BorderSymbolColor, this.BorderBackgroundColor,
                    PixelType.border);
            }
        }

        private void FillRightBorder()
        {
            int lastRow = (this.Width - 1);

            for (int i = 1; i < (this.Height - 1); i++)
            {
                Content[i, lastRow] = new Pixel(Symbol.VerticalLine, this.BorderSymbolColor, this.BorderBackgroundColor,
                    PixelType.border);
            }
        }

        private void FillEntryContent()
        {
            Pixel[,] entryContent = this.GetEntryContent();

            for (int i = 1; i <= this.EntryContentHeight; i++)
            {
                for (int j = 1; j <= this.EntryContentWidth; j++)
                {
                    entryContent[(i - 1), (j - 1)] = new Pixel(Symbol.Grass, ConsoleColor.Green, ConsoleColor.Black, PixelType.sprite);
                }
            }

            this.SetEntryContent(entryContent);
        }

        public Pixel[,] GetEntryContent()
        {
            Pixel[,] entryContent = new Pixel[this.EntryContentHeight,this.EntryContentWidth];

            for (int i = 1; i <= this.EntryContentHeight; i++)
            {
                for (int j = 1; j <= this.EntryContentWidth; j++)
                {
                    entryContent[(i - 1), (j - 1)] = Content[i, j];
                }
            }

            return entryContent;
        }

        public void SetEntryContent(Pixel[,] entryContent)
        {
            if (((entryContent.GetUpperBound(0) + 1) == this.EntryContentHeight) &&
                ((entryContent.GetUpperBound(1) + 1) == this.EntryContentWidth))
            {
                for (int i = 1; i <= this.EntryContentHeight; i++)
                {
                    for (int j = 1; j <= this.EntryContentWidth; j++)
                    {
                        this.Content[i, j] = entryContent[(i - 1), (j - 1)];
                    }
                }
            }
        }
    }
}
