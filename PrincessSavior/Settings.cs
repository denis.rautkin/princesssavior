﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Text;

namespace PrincessSavior
{
    public class Settings
    {
        public const int DefaultCellWidth = 10;
        public const int DefaultCellHeight = 5;
        public const int DefaultFieldWidth = 10;
        public const int DefaultFieldHeight = 10;
        public const int DefaultMinesCount = 10;
        public const int DefaultHealthLevel = 100;
        public const CellType DefaultCellType = CellType.ordinary;
        public const ConsoleColor DefaultBorderSymbolColor = ConsoleColor.White;
        public const ConsoleColor DefaultBorderBackgroundColor = ConsoleColor.Black;

        public const int FieldsStartWidth = 50;
        public const int FieldsStartHeight = 10;
        public const int MessageHeightPosition = 40;

        public const int HealthStatusHeightPosition = 5;

        public const int HIDE = 0;
        public const int MAXIMIZE = 3;
        public const int MINIMIZE = 6;
        public const int RESTORE = 9;

        [DllImport("kernel32.dll", ExactSpelling = true)]
        private static extern IntPtr GetConsoleWindow();
        private static IntPtr ThisConsole = GetConsoleWindow();
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        public static void ConfigureConsole()
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.SetWindowSize(Console.LargestWindowWidth, Console.LargestWindowHeight);
            ShowWindow(ThisConsole, MAXIMIZE);
        }
    }
}