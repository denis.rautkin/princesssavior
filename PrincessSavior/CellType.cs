﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrincessSavior
{
    public enum CellType
    {
        ordinary = 0,
        left,
        right,
        upperLeftCorner,
        upper,
        upperRightCorner,
        bottomLeftCorner,
        bottomRightCorner,
        bottom
    }
}
