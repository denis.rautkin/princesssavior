﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Transactions;

namespace PrincessSavior
{
    public class Game
    {
        public GameField CurrentGameField { get; set; }
        public Player OurPlayer { get; set; }
        public static Game CurrentGame { get; set; }

        public Game()
        {
            CurrentGame = this;
            CurrentGameField = new GameField(Settings.DefaultFieldWidth, Settings.DefaultFieldHeight, Settings.DefaultCellWidth, Settings.DefaultCellHeight, Settings.DefaultMinesCount);
            OurPlayer = new Player();
            OurPlayer.Health = Settings.DefaultHealthLevel;
        }

        public Game(int fieldWidth, int fieldHeight, int cellWidth, int cellHeight, int minesCount, Player player)
        {
            CurrentGame = this;
            CurrentGameField = new GameField(fieldWidth, fieldHeight, cellWidth, cellHeight, minesCount);
            OurPlayer = player;
            OurPlayer.Health = Settings.DefaultHealthLevel;
        }

        public void Run()
        {
            Settings.ConfigureConsole();
            CurrentGameField.SetPlayersPosition(OurPlayer);
            CurrentGameField.SetPrincessPosition();
            CurrentGameField.DropTraps(OurPlayer.Health);
            CurrentGameField.RenderGameField(this.OurPlayer);
            MovePlayerPosition();
        }

        public void GetMenu()
        {
            Console.Write("Menu:\n1) Play\n2) Exit\n");

            ConsoleKey choice = Console.ReadKey(true).Key;

            switch (choice)
            {
                case ConsoleKey.D1:
                    this.Run();
                    break;
                case ConsoleKey.NumPad1:
                    this.Run();
                    break;
                case ConsoleKey.D2:
                    Environment.Exit(0);
                    break;
                case ConsoleKey.NumPad2:
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Unrecognized choice! Try again!");
                    Console.ReadKey();
                    Console.Clear();
                    this.GetMenu();
                    break;
            }
        }

        public void MovePlayerPosition()
        {
            ConsoleKey direction = Console.ReadKey(true).Key;
            switch (direction)
            {
                case ConsoleKey.UpArrow:

                    MoveUp();

                    break;

                case ConsoleKey.DownArrow:

                    MoveDown();

                    break;

                case ConsoleKey.LeftArrow:

                    MoveLeft();

                    break;

                case ConsoleKey.RightArrow:

                    MoveRight();

                    break;

                default:
                    CurrentGameField.RenderMessageLine("Inputed direction is incorrect! Try again!");
                    Console.ReadKey();
                    this.MovePlayerPosition();
                    break;
            }

            void MoveUp()
            {
                if (this.OurPlayer.CurrentHeightPosition > 0)
                {
                    this.CurrentGameField
                        .FieldCells[this.OurPlayer.CurrentHeightPosition, this.OurPlayer.CurrentWidthPosition]
                        .IsPlayersCurrentPosition = false;
                    this.OurPlayer.CurrentHeightPosition -= 1;
                    this.CurrentGameField
                        .FieldCells[this.OurPlayer.CurrentHeightPosition, this.OurPlayer.CurrentWidthPosition]
                        .IsPlayersCurrentPosition = true;

                    CheckForTrap();
                    CheckForFinish();

                    this.CurrentGameField.RenderGameField(this.OurPlayer);
                }
                else
                {
                    CurrentGameField.RenderMessageLine("Player can't move to inputed direction!");
                }
                this.MovePlayerPosition();
            }

            void MoveDown()
            {
                if (this.OurPlayer.CurrentHeightPosition < (this.CurrentGameField.FieldHeight - 1))
                {
                    this.CurrentGameField
                        .FieldCells[this.OurPlayer.CurrentHeightPosition, this.OurPlayer.CurrentWidthPosition]
                        .IsPlayersCurrentPosition = false;
                    this.OurPlayer.CurrentHeightPosition += 1;
                    this.CurrentGameField
                        .FieldCells[this.OurPlayer.CurrentHeightPosition, this.OurPlayer.CurrentWidthPosition]
                        .IsPlayersCurrentPosition = true;

                    CheckForTrap();
                    CheckForFinish();

                    this.CurrentGameField.RenderGameField(this.OurPlayer);
                }
                else
                {
                    CurrentGameField.RenderMessageLine("Player can't move to inputed direction!");
                }
                this.MovePlayerPosition();
            }

            void MoveLeft()
            {
                if (this.OurPlayer.CurrentWidthPosition > 0)
                {
                    this.CurrentGameField
                        .FieldCells[this.OurPlayer.CurrentHeightPosition, this.OurPlayer.CurrentWidthPosition]
                        .IsPlayersCurrentPosition = false;
                    this.OurPlayer.CurrentWidthPosition -= 1;
                    this.CurrentGameField
                        .FieldCells[this.OurPlayer.CurrentHeightPosition, this.OurPlayer.CurrentWidthPosition]
                        .IsPlayersCurrentPosition = true;

                    CheckForTrap();
                    CheckForFinish();

                    this.CurrentGameField.RenderGameField(this.OurPlayer);
                }
                else
                {
                    CurrentGameField.RenderMessageLine("Player can't move to inputed direction!");
                }
                this.MovePlayerPosition();
            }

            void MoveRight()
            {
                if (this.OurPlayer.CurrentWidthPosition < (this.CurrentGameField.FieldWidth - 1))
                {
                    this.CurrentGameField
                        .FieldCells[this.OurPlayer.CurrentHeightPosition, this.OurPlayer.CurrentWidthPosition]
                        .IsPlayersCurrentPosition = false;
                    this.OurPlayer.CurrentWidthPosition += 1;
                    this.CurrentGameField
                        .FieldCells[this.OurPlayer.CurrentHeightPosition, this.OurPlayer.CurrentWidthPosition]
                        .IsPlayersCurrentPosition = true;

                    CheckForTrap();
                    CheckForFinish();

                    this.CurrentGameField.RenderGameField(this.OurPlayer);
                }
                else
                {
                    CurrentGameField.RenderMessageLine("Player can't move to inputed direction!");
                }
                this.MovePlayerPosition();
            }

            void CheckForTrap()
            {
                Cell currentPlayerPosition = this.CurrentGameField
                    .FieldCells[this.OurPlayer.CurrentHeightPosition, this.OurPlayer.CurrentWidthPosition];
                if (currentPlayerPosition.Trap != null)
                {
                    if (currentPlayerPosition.Trap.IsActive == true)
                    {
                        Trap trap = currentPlayerPosition.Trap;
                        this.OurPlayer.CurrentHealth -= trap.DamagePoints;
                        this.CurrentGameField
                            .FieldCells[this.OurPlayer.CurrentHeightPosition, this.OurPlayer.CurrentWidthPosition].Trap.IsActive = false;
                        CurrentGameField.RenderMessageLine($"Player has got damage {trap.DamagePoints} points. Current health is {this.OurPlayer.CurrentHealth} points. Be careful!");
                        
                        Console.ReadKey();
                    }
                }

                if (this.OurPlayer.CurrentHealth <= 0)
                {
                    CurrentGameField.RenderMessageLine("Game over!!! Press any key to exit in main menu.");
                    Console.ReadKey();
                    Console.Clear();
                    CurrentGame = new Game();
                    CurrentGame.GetMenu();
                }
            }

            void CheckForFinish()
            {
                Cell currentPlayerPosition = this.CurrentGameField
                    .FieldCells[this.OurPlayer.CurrentHeightPosition, this.OurPlayer.CurrentWidthPosition];
                if (currentPlayerPosition.IsPrincessPosition == true)
                {
                  
                        CurrentGameField.RenderMessageLine("You win!!! Congratulations!!! Princes is saved!!!");
                        Console.ReadKey();
                        Console.Clear();
                        CurrentGame = new Game();
                        CurrentGame.GetMenu();
                }
            }
        }

    }
}
