﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrincessSavior
{
    public class Trap
    {
        public int DamagePoints { get; set; }
        public bool IsActive { get; set; }

        public Trap()
        {
            this.IsActive = true;
        }
    }
}
